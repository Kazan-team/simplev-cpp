# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information
.PHONY: all tests clean fix-tests

CC = powerpc64le-linux-gnu-gcc
CXX = powerpc64le-linux-gnu-g++
CFLAGS = -O3 -Iinclude -g0 -mno-altivec -mno-vsx -Wall
CXXFLAGS = -std=gnu++17

all: tests

TESTS_SOURCE := $(wildcard tests/*/test.cpp) $(wildcard tests/*/test.c)
TESTS_DIR := $(dir $(TESTS_SOURCE))
TESTS_BUILD_DIR := $(addprefix build/,$(TESTS_DIR))
TESTS_DIFF := $(addsuffix diff.txt,$(TESTS_BUILD_DIR))
TESTS_FILTERED_OUT := $(addsuffix filtered-out.s,$(TESTS_BUILD_DIR))
EXTRA_DEPS := Makefile $(wildcard include/*.h)

build/tests/%/:
	mkdir -p $@

build/tests/%/out.s: tests/%/test.cpp $(EXTRA_DEPS) | build/tests/%/
	$(CXX) -S $(CFLAGS) $(CXXFLAGS) $< -o $@

build/tests/%/out.s: tests/%/test.c $(EXTRA_DEPS) | build/tests/%/
	$(CC) -S $(CFLAGS) $< -o $@

build/tests/%/filtered-out.s: build/tests/%/out.s $(EXTRA_DEPS)
	sed 's/\(^\t.ident\t"\).*"/\1GCC"/' < $< > $@

build/tests/%/diff.txt: tests/%/expected.s build/tests/%/filtered-out.s $(EXTRA_DEPS)
	diff -u $< build/$(dir $<)filtered-out.s > $@ || true

.PRECIOUS: build/tests/%/out.s build/tests/%/filtered-out.s build/tests/%/

tests: $(TESTS_DIFF)
	@failed=0; \
	for i in $+; do \
		if [ -s "$$i" -o ! -e "$$i" ]; then \
			echo "Test failed: $$i" >&2; \
			head -n 10 "$$i" >&2; \
			failed=$$((failed + 1)); \
		fi; \
	done; \
	if [ $$((failed)) != 0 ]; then \
		echo "$$failed test(s) failed" >&2; \
		false; \
	else \
		echo "all tests passed"; \
	fi

fix-tests: $(TESTS_FILTERED_OUT)
	@for i in $(TESTS_FILTERED_OUT); do \
		target="$$(dirname "$${i##build/}")/expected.s"; \
		status=""; \
		[ "$(FORCE_FIX_TESTS)" = "$$target" ] || status="$$(git status --porcelain "$$target")"; \
		if [ -z "$$status" ]; then \
			cp -v "$$i" "$$target"; \
		elif ! cmp "$$i" "$$target"; then \
			echo "$$target has uncommitted changes, not overwriting -- commit changes or run make with FORCE_FIX_TESTS=$$target" >&2; \
			exit 1; \
		fi; \
	done

clean:
	rm -rf build/tests