// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information

#include <cstddef>
#include <cstdint>

#include "simplev_cpp.h"

using u8x4 = sv::Vec<std::uint8_t, 1, 4>;
using u16x4 = sv::Vec<std::uint16_t, 1, 4>;

u8x4 test_sub_1(u8x4 a, u8x4 b)
{
    return sv::sub(a, b);
}

u16x4 test_sub_2(u16x4 a, u16x4 b)
{
    return sv::sub(a, b);
}

u16x4 test_sub_3(u16x4 a, u16x4 b, u16x4 c)
{
    return sv::sub(a, sv::sub(b, c));
}